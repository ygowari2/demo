import sqlite3
Connection = sqlite3.connect('items.db')
cursor = Connection.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS items(id integer primary key autoincrement, item TEXT,status TEXT)")

print("Table created successfully........")

# Commit your changes in the database
Connection.commit()

#Closing the connection
Connection.close()