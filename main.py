from flask import Flask, json,jsonify,request
from flask_sqlalchemy import SQLAlchemy
import sqlite3
import concurrent.futures
import time

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///items.db'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

@app.route('/item', methods=['POST'])
def item():
    credentials = request.get_json()
    item = credentials["item"]
    status = "pending"
    Connection = sqlite3.connect('items.db')
    cursor = Connection.cursor()
    cursor.execute('INSERT INTO items(status,item)VALUES(?,?)',(status,item))
    Connection.commit()
    Connection = sqlite3.connect('items.db')
    cursor = Connection.cursor()
    cursor.execute("select max(id)from items")
    id = cursor.fetchone()
    cursor.execute("select item from items where id in(select max(id) from items)")
    item = cursor.fetchone()
    cursor.execute("select status from items where id in(select max(id) from items)")
    status = cursor.fetchone()
    cursor.execute("select id,item,status from items Where item='Book'")
    data = cursor.fetchone()
    new = {'id':id,'item':item,'status':status}
    return jsonify(message = new,statusCode= 200),200

@app.route('/delay/', methods=['GET'])
def login():

    data = request.get_json()
    delay_value = data['delay_value']
    start = time.perf_counter()

    sec = []
    sec = [delay_value] * 5
    
    def task(seconds):
        print(f'Sleeping')
        time.sleep(delay_value)
        print('Done Sleeping...')

    with concurrent.futures.ThreadPoolExecutor() as executor:
            results = executor.map(task,sec)

    finish = time.perf_counter()
    new = f'Finished in {round(finish-start, 2)} second(s)'
    return jsonify(message = new,statusCode= 200),200

if __name__ == "__main__":
    app.run(debug=True,port=500)